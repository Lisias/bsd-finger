/*
 * String to embed in binaries to identify package
 */

#define _PKG_VERSION_ "0.18.2-Lisias"
char pkg[]="$NetKit: bsd-finger-" _PKG_VERSION_ " $";
char ver[]=_PKG_VERSION_;
