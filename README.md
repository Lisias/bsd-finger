# bsd finger

Patches from [bsd-finger 0.17-15.1.debian](ftp://ftp.linux.org.uk/pub/linux/Networking/netkit/bsd-finger_0.17-15.1.debian.tar.gz)) applied.

Changes from Lisias applied.

## Padrão de Projeto

* Código fonte:
	* Hard TABs - um TAB por nível de identação, indiferente da linguagem
		* TABs não devem ser usados novamente na linha após o primeiro *non-whitespace char*.
			* Depois que digitou algo, só use espaços
		* Espaços depois dos TABs e antes do primeiro *non-whitespace char* devem ser evitados.
		* Em Python, isso vai poupar muita dor de cabeça.
	* UNIX Line Endings
	* UTF-8.
* Documentação
	* MarkDown em tudo onde qualquer outro formado não for estritamente necessário


## Gerência de Configuração

* Instalar
	* Eclipse 2018-09 (ou superior)
		* CDT
		* AnyTools
		* EasyShell
		* LiClipseText
		* Todos disponívels do Eclipse MarketPlace
* Configurar
	* nada no momento
* Ferramentas auxiliares sugeridas:
	* Windows
		* [NotePad++](https://notepad-plus-plus.org/download/v7.5.9.html)
		* [CygWin](https://www.cygwin.com/)
		* [Markdown Pad](http://markdownpad.com/)
	* MacOS
		* [MacPorts](https://www.macports.org/)
		* [MacDown](https://macdown.uranusjr.com/)
		* [BBEdit](https://www.barebones.com/products/bbedit/)
	* Linux
		+ TDB
* Notas
	* MacOS
		* A Apple cagou a stdlib, criando símbolos que a especificação exige que estejam disponíveis para a userland.
			* Isso impede que o finged seja compilado e testado no MacOS, apesar dele ser mais UNIX que o próprio Linux. 	 


## Referências

* linux org
	+ [ftp](ftp://ftp.linux.org.uk/pub/linux/Networking/finger)
* Debian
	+ [patches](http://deb.debian.org/debian/pool/main/b/bsd-finger/bsd-finger_0.17-15.1.debian.tar.bz2)
